evolis-emulator is a Python GUI utility for emulating Evolis Card Printers.

It was written as a fast way to test development of drivers and software integration, without wasting time and resources printing actual cards.

As always, this utility is best-effort and may not accurately reflect the behaviour of any partictular Evolis printer. Use it to speed up development and testing, but always check the final result with the real hardware. :)

This software is currently ***PRE-ALPHA***
