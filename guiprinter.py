#!/usr/bin/env python
import pygame, socket, select, struct, math

cardsize = (648,1016)

class EvolisEmulator():
    def __init__(self):
        pass
        self.name = "Evolis Printer Emulator"
        self.card_width = 648
        self.card_height = 1016
        self.thumb_width = 100
        self.thumb_height = 156
        self.bar_width = 384
        self.width = self.card_width * 2 + self.bar_width
        self.height = self.card_height
        self.blank = [[[0 for i in range(0,self.width)] for j in range(0,self.height)] for k in [0,1]]
        self.current = [None,None,{"layers":["",""]}]
        self.clear_current()
        self.thumbs = [[None,None,self.current[2]]]
        self.pages = []
        self.display_page = -1
        self.display_side = 1
        self.init_statemachine()
        LOGOCOLOUR = (4,4,4)
        CARDCOLOUR = (192,64,64)
        self.icon_size = 128
        icon = pygame.Surface((self.icon_size,self.icon_size), pygame.SRCALPHA)
        pygame.draw.circle(icon, CARDCOLOUR,(22+84-8,8),8, 0)
        pygame.draw.circle(icon, CARDCOLOUR,(22+8,8),8, 0)
        pygame.draw.circle(icon, CARDCOLOUR,(22+8,127-8),8, 0)
        pygame.draw.circle(icon, CARDCOLOUR,(22+84-8,127-8),8, 0)
        pygame.draw.rect(icon, CARDCOLOUR,(22+8,0,84-16,128), 0)
        pygame.draw.rect(icon, CARDCOLOUR,(22,8,84,127-16), 0)
        pygame.draw.arc(icon, (255,255,255),(22+84-16,0,16,16), 0, math.pi/2, 1)
        pygame.draw.arc(icon, (255,255,255),(22,0,16,16), math.pi/2, math.pi, 1)
        pygame.draw.arc(icon, (255,255,255),(22,127-16,16,16), math.pi,3*math.pi/2, 1)
        pygame.draw.arc(icon, (255,255,255),(22+84-16,127-16,16,16), 3*math.pi/2, 2*math.pi, 1)
        pygame.draw.line(icon, (255,255,255),(22+8,0),(22+84-8,0), 1)
        pygame.draw.line(icon, (255,255,255),(22+8,127),(22+84-8,127), 1)
        pygame.draw.line(icon, (255,255,255),(22,8),(22,127-8), 1)
        pygame.draw.line(icon, (255,255,255),(22+84,8),(22+84,127-8), 1)

        pygame.draw.arc(icon,LOGOCOLOUR,(48-15,105-15,31,31),0,math.pi/2,1)
        pygame.draw.arc(icon,LOGOCOLOUR,(48-15,105-15,31,31),math.pi,3*math.pi/2,1)
        pygame.draw.arc(icon,LOGOCOLOUR,(48-7,105-15,15,31),0,math.pi/2,1)
        pygame.draw.arc(icon,LOGOCOLOUR,(48-7,105-15,15,31),math.pi,3*math.pi/2,1)
        pygame.draw.arc(icon,LOGOCOLOUR,(48-15,105+7,31,15),math.pi/2,7*math.pi/9,1)
        pygame.draw.arc(icon,LOGOCOLOUR,(48-15,105-22,31,15),3*math.pi/2,16*math.pi/9,1)
        pygame.draw.rect(icon,LOGOCOLOUR,(48-16,105-16,16,16),1)
        pygame.draw.rect(icon,LOGOCOLOUR,(48+0,105+0,16,16),1)
        font = pygame.font.Font(None, 62)
        text = font.render("PRINT",True,(96,0,192))
        icon.blit(text, (64-(text.get_width()/2),0))
        font = pygame.font.Font(None, 88)
        text = font.render("EMU",True,(96,0,192))
        icon.blit(text, (64-(text.get_width()/2),34))
        pygame.display.set_icon(icon)
        self.icon = icon

    def load_cardstock(self,firstlayer='k'):
        self.pixelarray = None
        stock = pygame.Surface((self.card_width, self.card_height))
        stock.fill((255,255,255))
        try:
            if firstlayer == 'k':
                img = pygame.image.load("cardstock_"+("back" if self.side else "front")+".png")
                stock.blit(img,(0,0))
        except:
            ""

        for line in xrange(0,self.card_height):
            for col in xrange(0,self.card_width):
                (R, G, B, A) = stock.get_at((self.card_width-col-1,self.card_height-line-1))
                self.C[self.side][line][col] = 255-R
                self.M[self.side][line][col] = 255-G
                self.Y[self.side][line][col] = 255-B
        self.current[self.side] = pygame.Surface((self.card_width, self.card_height))
        self.current[self.side].blit(stock,(0,0))

    def clear_current(self):
        import time
        print(repr(time.time())+" blanking start")
        self.C = [[[0 for i in range(0,self.width)] for j in range(0,self.height)] for k in [0,1]]
        self.M = [[[0 for i in range(0,self.width)] for j in range(0,self.height)] for k in [0,1]]
        self.Y = [[[0 for i in range(0,self.width)] for j in range(0,self.height)] for k in [0,1]]
        self.K = [[[0 for i in range(0,self.width)] for j in range(0,self.height)] for k in [0,1]]
        print(repr(time.time())+" blanking end")
        self.current = [None,None,{"layers":["",""]}]
        self.printed = False

    def save_current(self):
        self.pixelarray = None
        if self.current[self.side]:
            self.thumbs[-1][self.side] = pygame.transform.smoothscale(self.current[self.side], (self.thumb_width,self.thumb_height))
        if not self.current[0]:
            self.current[0] = pygame.Surface((self.card_width, self.card_height))
            self.current[0].fill((255,255,255))
	    self.thumbs[-1][0] = pygame.transform.scale(self.current[0], (self.thumb_width,self.thumb_height))
        if self.side == 1 and not self.current[1]:
            self.current[1] = pygame.Surface((self.card_width, self.card_height))
            self.current[1].fill((255,255,255))
	    self.thumbs[-1][1] = pygame.transform.scale(self.current[1], (self.thumb_width,self.thumb_height))

        self.thumbs[-1][2] = self.current[2] # metadata
        self.pages += [self.current]
        if self.display_page < 0 and self.printed:
                self.display_page = -1
                self.display_side = self.side
        self.clear_current()
        self.thumbs += [[None,None,self.current[2]]]

    def process(self, chunk):
        for data in chunk:
            self.statemachine(data)
            self.offs += 1
        self.pixelarray = None

    def keypress(self, key):
        print("KEYPRESS: "+repr(key))
        maxthumb = len(self.thumbs)-1
        if self.thumbs[-1][:2] == [None,None]:
            maxthumb -= 1
        thumbpage = self.display_page
        if thumbpage < 0:
            thumbpage = len(self.thumbs)-(3+thumbpage)

        if key == pygame.K_DOWN and thumbpage > 0:
            thumbpage -= 1
            self.display_page = thumbpage
        elif key == pygame.K_UP and thumbpage < maxthumb:
            thumbpage += 1
        elif key == pygame.K_LEFT:
            if self.display_side == 1:
                self.display_side = 0
            elif thumbpage > 0:
                self.display_side = 1
                thumbpage -= 1
        elif key == pygame.K_RIGHT:
            if self.display_side == 0:
                self.display_side = 1
            elif thumbpage < maxthumb:
                self.display_side = 0
                thumbpage += 1
        elif key == pygame.K_HOME:
            thumbpage = maxthumb
        elif key == pygame.K_END:
            thumbpage = 0
        elif key == pygame.K_PAGEUP:
            thumbpage += 5
            if thumbpage > maxthumb:
                thumbpage = maxthumb
        elif key == pygame.K_PAGEDOWN:
            thumbpage -= 5
            if thumbpage < 0:
                thumbpage = 0

        if thumbpage == len(self.thumbs)-1:
            self.display_page = -2
            if self.display_side == self.side or not self.thumbs[thumbpage][self.display_side]:
                self.display_side = 1-self.display_side
            if self.display_side == self.side or not self.thumbs[thumbpage][self.display_side]:
                self.display_side = self.side-1
                if self.display_side < 0:
                    self.display_page = -1
                    thumbpage = len(self.thumbs)-2
                    self.display_side = 1
        elif thumbpage == maxthumb:
            self.display_page = -1
        else: 
            self.display_page = thumbpage
        if not self.thumbs[thumbpage][self.display_side]:
            self.display_side = 0

    def render(self, screen):
        ""
        view_start = 0
        nav_start = self.card_width
        now_start = self.card_width+self.bar_width
        icon_start = self.card_width+24
        status_start = self.card_width+24+self.icon_size+24
        thumb_front = icon_start+((self.icon_size-self.thumb_width)/2)
        thumb_back = thumb_front+self.thumb_width+14
        thumb_stat = thumb_back+self.thumb_width+12
        smallfont = pygame.font.Font(None, 18)
        font = pygame.font.Font(None, 28)
        bigfont = pygame.font.Font(None, 48)
        # central navigation pane
        pygame.draw.rect(screen,(0,0,0),[nav_start,0,self.bar_width,self.card_height])
        screen.blit(self.icon,(icon_start,16));
        text = font.render("Current Side:",True,(255,255,255))
        screen.blit(text, (status_start,12))
        text = font.render(("None" if not self.printed else " ".join(["Card",str(len(self.pages)+1),("Back" if self.side else "Front")])),True,(160,160,160))
        screen.blit(text, (status_start,36))
        text = font.render("Current Command:",True,(255,255,255))
        screen.blit(text, (status_start,64))
        text = font.render(";".join([str(self.command)]+self.params)+(" ..." if self.bytecounter else ""),True,(160,160,160))
        screen.blit(text, (status_start,88))

        # render history
        if self.current[self.side]:
            self.thumbs[-1][self.side] = pygame.transform.smoothscale(self.current[self.side], (self.thumb_width,self.thumb_height))

        numthumbs = len(self.thumbs)
        if self.thumbs[-1][:2] == [None,None]:
            numthumbs -= 1
        thumbpage = self.display_page
        if thumbpage < 0:
            thumbpage = len(self.thumbs)-(3+thumbpage)
        thumbline = 160
        showthumbs = range(0,numthumbs)[:thumbpage+3][-5:]
        if thumbpage < 3:
            showthumbs = range(0,numthumbs)[:5]
        for pagenum in reversed(showthumbs):
            page = self.thumbs[pagenum]
            if page[0]:
                pygame.draw.rect(screen, (128,128,128),(thumb_front-2,thumbline-2,self.thumb_width+4,self.thumb_height+4), 3)
                screen.blit(page[0], (thumb_front,thumbline))
            if page[1]:
                pygame.draw.rect(screen, (128,128,128),(thumb_back-2,thumbline-2,self.thumb_width+4,self.thumb_height+4), 3)
                screen.blit(page[1], (thumb_back,thumbline))
            if pagenum == thumbpage:
                pygame.draw.rect(screen, (64,255,64),((thumb_back if self.display_side else thumb_front)-6,thumbline-6,self.thumb_width+12,self.thumb_height+12), 4)
                pairs = (
                    (thumb_front-16, thumbline+(self.thumb_height*2/5)),
                    (nav_start+6, thumbline+(self.thumb_height/2)),
                    (thumb_front-16, thumbline+(self.thumb_height*3/5))
                    )
                pygame.draw.lines(screen, (64,192,64),False,pairs,6)
            if self.printed and pagenum == len(self.thumbs)-1:
                arrow_width = thumb_front - nav_start
                pairs = (
                    (now_start-arrow_width+16, thumbline+(self.thumb_height*2/5)),
                    (now_start-6, thumbline+(self.thumb_height/2)),
                    (now_start-arrow_width+16, thumbline+(self.thumb_height*3/5)),
                    )
                pygame.draw.lines(screen, (96,96,96),False,pairs,6)
            text = bigfont.render(str(pagenum+1),True,(160,160,160))
            thumb_centre = thumbline+((self.thumb_height-text.get_height())/2)
            screen.blit(text,(thumb_stat,thumb_centre))
            text = font.render("Card Num:",True,(255,255,255))
            screen.blit(text,(thumb_stat,thumb_centre-24))
            text = smallfont.render(self.thumbs[pagenum][2]["layers"][0].upper(),True,(160,160,160))
            screen.blit(text,(thumb_stat,thumb_centre+40))
            text = smallfont.render(self.thumbs[pagenum][2]["layers"][1].upper(),True,(160,160,160))
            screen.blit(text,(now_start-24-text.get_width(),thumb_centre+40))

            thumbline += 170

        # view pane
        if self.display_page == -2:
            if self.current[self.display_side]:
                screen.blit(self.current[self.display_side],(view_start,0));
        elif self.pages and self.pages[self.display_page][self.display_side]:
            screen.blit(self.pages[self.display_page][self.display_side],(view_start,0));
        # current pane
        if self.current[self.side]:
            screen.blit(self.current[self.side],(now_start,0));
        else:
            hugefont = pygame.font.Font(None, 72)
            text = hugefont.render("NO CARD PRINTING",True,(160,160,160))
            screen.blit(text,(now_start+((self.card_width-text.get_width())/2),(self.card_height-text.get_height())/2));

    def updatepixel(self, line, col):
        if not self.pixelarray:
            self.pixelarray = pygame.PixelArray(self.current[self.side])
        R = (255-self.C[self.side][line][col])*(255-self.K[self.side][line][col])/255
        G = (255-self.M[self.side][line][col])*(255-self.K[self.side][line][col])/255
        B = (255-self.Y[self.side][line][col])*(255-self.K[self.side][line][col])/255
        self.pixelarray[self.card_width-col-1,self.card_height-line-1] = (R, G, B)
        ""

    def init_statemachine(self):
        self.init_command()
        self.side = 0
        self.layer = None
        self.offs = 0

    def init_command(self):
        self.command = None
        self.commandfunc = None
        self.params = []
        self.buffer = ""
        self.bytecounter = None
        self.line = 0
        self.col = 0

    def simple_command(self, command, params):
        self.pixelarray = None
        if command == 'Sr':
            if self.display_page < 0 and self.printed and self.side != 0:
                self.display_page = -2
                self.display_side = self.side
            self.side = 0 # Front
            result = "Select front side"
            self.printed = True
        elif command == 'Sv':
            if self.display_page < 0 and self.printed and self.side != 1:
                self.display_page = -2
                self.display_side = self.side
            self.side = 1 # Back
            result = "Select back side"
            self.printed = True
        elif command == 'Ss':
            if self.display_page < 0 and self.printed and self.side != 0:
                self.display_page = -2
                self.display_side = self.side
            self.side = 0
            result = "Start command sequence"
        elif command == 'Si':
            if self.display_page < 0 and self.printed and self.side != 0:
                self.display_page = -2
                self.display_side = self.side
            self.side = 0
            result = "Load card"
            self.printed = True
        elif command == 'Se':
            self.save_current()
            result = "End command sequence and eject card"
        else:
            result = "UNKNOWN COMMAND"
         # if current selected in display pane, always show non-printing side.
        print(str(self.offs)+" "+"COMMAND: "+repr(command)+" "+repr(params)+" #"+result)

    def init_complex_command(self, command, params):
        if self.command == "Db" and len(self.params) == 2:
            print(str(self.offs)+" "+"COMMAND: "+repr(command)+" "+repr(params)+" ...")
            self.commandfunc = self.progressive_bitmap
            # Download Bitmap
            self.current[2]["layers"][self.side] += params[0]
            if params[0] == 'c':
                self.layer = self.C[self.side]
            elif params[0] == 'm':
                self.layer = self.M[self.side]
            elif params[0] == 'y':
                self.layer = self.Y[self.side]
            elif params[0] == 'k':
                self.layer = self.K[self.side]
            else:
                print("UNSUPPORTED LAYER "+repr(params[0]))
                self.layer = None # ignore this layer
            if params[1] == '2':
                bitsize = 1
            elif params[1] == '32':
                bitsize = 5
            elif params[1] == '64':
                bitsize = 6
            elif params[1] == '128':
                bitsize = 7
            elif params[1] == '256':
                bitsize = 8
                self.commandfunc = self.progressive_bitmap_256
            else:
                print("UNSUPPORTED COLOUR DEPTH "+repr(params[1]))
                self.command = "IGNORE" #unknown bit depth
                return

            self.bytecounter = 648*1016*bitsize/8
            self.chunksize = bitsize # always 8-pixel chunks`
            if not self.current[self.side]:
                self.load_cardstock(params[0])

    def complex_command(self, command, params, data):
        print(str(self.offs)+" "+"UNHANDLED COMPLEX COMMAND: "+repr(command)+" "+repr(params)+" +"+str(len(data)))
        "" # do nothing (for now)

    def dolayerpixel(self, pixel):
        self.layer[self.line][self.col] = pixel
        self.updatepixel(self.line, self.col)
        # move to next pixel
        self.col += 1
        if self.col == self.card_width:
            self.col = 0
            self.line += 1

    def progressive_bitmap(self, data):
        if self.layer == None:
            return
        # always 8 pixels at once
        bitdata = struct.unpack("<Q", (data+"\0\0\0\0\0\0\0")[:8])[0]
        bits = self.chunksize # self.chunksize is always the bit size
        bitmask = (1<<bits)-1
        bitshift = 8-bits
        for bytenum in reversed(range(0,8)):
            pixel = ((bitdata>>(bytenum*bits))&bitmask)<<bitshift
            # copy value down into empty bits
            if bits == 1:
                pixel = 255 if pixel else 0
            else:
                pixel |= pixel>>bits
            self.dolayerpixel(pixel)

    def progressive_bitmap_256(self, data):
        if self.layer == None:
            return
        for pixel in data:
            self.dolayerpixel(ord(pixel))

    def statemachine(self, data):
        if not self.command:
            if data == '\033': #ESC
                if self.buffer:
                    print(str(self.offs)+" "+"JUNK: "+repr(self.buffer))
                self.init_command() #fresh command.
            elif data == '\r': #end of command with no params
                if self.buffer:
                    self.simple_command(self.buffer, [])
                self.init_command() #fresh command.
            elif data == ';': # start of params
                self.command = self.buffer
                self.buffer = ""
            else:
                self.buffer += data
        elif self.command == "IGNORE":
            if data == '\033': #ESC
                self.init_command() #fresh command.
        elif self.bytecounter:
            self.buffer += data
            self.bytecounter -= 1
            if not self.bytecounter or (self.chunksize and len(self.buffer) == self.chunksize):
                if self.commandfunc:
                    self.commandfunc(self.buffer)
                    self.buffer = ""
            if not self.bytecounter:
                if self.buffer:
                    self.complex_command(self.command, self.params, self.buffer)
                self.init_command()
        elif data == ';':
            self.params += [self.buffer]
            self.buffer = ""
            self.init_complex_command(self.command, self.params)
        elif data == '\000':
            print(str(self.offs)+" "+"UNKNOWN DATA AFTER: "+repr(self.command)+" "+repr(self.buffer))
            self.command = "IGNORE"
        elif data == '\r': #end of unknown or ignored command
            self.params += [self.buffer]
            self.simple_command(self.command, self.params)
            self.init_command() #fresh command.
        else:
            self.buffer += data
        #

class PrinterEmulator():
    def __init__(self):
        pass
        pygame.init()
        self.emulator = EvolisEmulator()
        width, height = (self.emulator.width,self.emulator.height)
        self.screen = pygame.display.set_mode((width, height))
        pygame.display.set_caption(self.emulator.name or "Printer Emulator")
        self.clock = pygame.time.Clock()
        self.serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.serversocket.bind(("localhost",9100))
        self.serversocket.listen(1)
        self.clientsocket = None

    def update(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit()
            elif event.type == pygame.KEYDOWN:
                self.emulator.keypress(event.key)
            elif event.type != pygame.KEYUP and event.type != pygame.MOUSEMOTION:
                print(repr(event))
        toread, towrite, toerr = select.select(
            [self.clientsocket or self.serversocket],
            [],
            [self.clientsocket or self.serversocket],
            0.01
            )
        if toread:
            if not self.clientsocket:
                (self.clientsocket, address) = self.serversocket.accept()
            else:
                chunk = self.clientsocket.recv(256)
                if chunk == '':
                        toerr = (self.clientsocket)
                else:
                        self.emulator.process(chunk)
        else:
            self.clock.tick(60) #60FPS
        if toerr:
            self.clientsocket.close()
            self.clientsocket = None
        self.screen.fill((128,128,128)) #CLS
        self.emulator.render(self.screen)
        pygame.display.flip() #update screen

app = PrinterEmulator()
while 1:
    app.update()

